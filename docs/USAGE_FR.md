# Webservice ph5-dataselect

## Description

Ce service donne accès aux données sismiques archivées au format PH5.

## Formats de sorties disponibles

  - miniSEED
  - SAC binary
  - GEOCSV (TSPAIR)

## Utilisation de la requête

    /query? (channel-options) (date-range-options) [output-options] [filter-options] [nodata=404]

    où :

    channel-options      ::  (net=<network>) (sta=<station>) (loc=<location>) (cha=<channel>)
    date-range-options   ::  (starttime=<date|durée>) (endtime=<date|durée>)
    output-options       ::  [format=<geocsv|mseed|sac>]
    filter-options       ::  [decimate=<2-16>] [reduction=<number>]

    (..) requis
    [..] optionnel

## Exemples de requêtes

<a href="http://ph5ws.resif.fr/fdsnws/dataselect/1/query?net=3C&station=N02&channel=DPN&start=2019-11-13T12:00:00&end=2019-11-13T12:50:00&nodata=404&format=sac">http://ph5ws.resif.fr/fdsnws/dataselect/1/query?net=3C&station=N02&channel=DPN&start=2019-11-13T12:00:00&end=2019-11-13T12:50:00&nodata=404&format=sac</a>

## Descriptions détaillées de chaque paramètre de la requête

### Choix du capteur

| Paramètre  | Exemple | Discussion                                                                    |
| :--------- | :------ | :---------------------------------------------------------------------------- |
| net[work]  | FR      | Nom du réseau sismique. Accepte les jokers et les listes.                     |
| sta[tion]  | CIEL    | Nom de la station. Accepte les jokers et les listes.                          |
| loc[ation] | 00      | Code de localisation. Utilisez loc=-- pour les codes de localisations vides. Accepte les jokers et les listes. |
| cha[nnel]  | HHZ     | Code de canal. Accepte les jokers et les listes.                              |

#### Jokers et listes d'arguments

  - Jokers : le point d’interrogation __?__ représente n'importe quel caractère unique, alors que l'astérisque __*__ représente zéro caractère ou plus.

  - Listes : plusieurs éléments peuvent être récupérés à l'aide d'une liste séparée par des virgules. Les jokers peuvent être inclus dans la liste.

Par exemple, pour le code des canaux : channel=EH?,BHZ

### Choix de l'intervalle de temps

| Paramètre  | Exemple | Discussion                                                                  |
| :--------- | :------ | :-------------------------------------------------------------------------- |
| start[time] | 2010-01-10T00:00:00 | Sélectionne les données à partir de l'heure spécifiée incluse. |
| end[time]  | 2011-02-11T01:00:00 | Sélectionne les données avant l'heure spécifiée incluse.        |

La définition de l'intervalle de temps avec starttime et endtime peut prendre différentes formes :

  - une date, par exemple starttime=2015-08-12T01:00:00
  - une durée en secondes, par exemple endtime=7200
  - le mot-clé "currentutcday" qui signifie minuit de la date du jour (UTC), par exemple starttime=currentutcday

### Options de traitements des signaux temporels

| Paramètre  | Exemple |                              Discussion                                   | Valeur par défaut |
| :--------- | :------ | :------------------------------------------------------------------------ | :----- |
| reduction  | 1.5     | Réduis la vitesse du signal par une constante.                            | aucune |
| deci[mate] | 2.0     | Facteur de décimation.                                                    | aucune |

### Options de sortie
| Paramètre | Exemple |                                 Discussion                                   | Valeur par défaut |
| :-------- | :------ | :--------------------------------------------------------------------------- | :----- |
| format    |   sac   | Format de sortie du fichier : geocsv, mseed, sac                             | mseed  |
| nodata    |   404   | Code d'état HTTP qui est renvoyé lorsqu'il n'y a pas de données (204 or 404) | 204    |

## Requêtes HTTP POST

La forme générale d'une requête POST est un ensemble de paires parameter=value, une par ligne, suivie d'un nombre arbitraire de canaux et d'une fenêtre temporelle optionnelle :

parameter=\<value\> \
parameter=\<value\> \
parameter=\<value\> \
Net Sta Loc Chan [StartTime EndTime] \
Net Sta Loc Chan [StartTime EndTime] \
...

La fenêtre temporelle peut être précisée globalement :

... \
start=2020-10-01T00:00:00 \
end=2020-10-01T00:01:00 \
Net1 Sta1 Loc1 Chan1 \
Net2 Sta2 Loc2 Chan2 \
...

ou par ligne :

... \
Net1 Sta1 Loc1 Chan1 2020-10-01T00:00:00 2020-10-01T00:01:00 \
Net2 Sta2 Loc2 Chan2 2020-10-02T00:00:00 2020-10-02T00:02:00 \
...

La fenêtre temporelle globale peut être utilisée conjointement avec des fenêtres temporelles individuelles. Si aucune information temporelle n’est précisée, la période maximale de disponibilité est utilisée.

## Formats des dates et des heures

    YYYY-MM-DDThh:mm:ss[.ssssss] ex. 1997-01-31T12:04:32.123
    YYYY-MM-DD ex. 1997-01-31 (une heure de 00:00:00 est supposée)

    avec :

    YYYY    :: quatre chiffres de l'année
    MM      :: deux chiffres du mois (01=Janvier, etc.)
    DD      :: deux chiffres du jour du mois (01 à 31)
    T       :: séparateur date-heure
    hh      :: deux chiffres de l'heure (00 à 23)
    mm      :: deux chiffres des minutes (00 à 59)
    ss      :: deux chiffres des secondes (00 à 59)
    ssssss  :: un à six chiffres des microsecondes en base décimale (0 à 999999)

