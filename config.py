import os


class Config:

    """
    Configure Flask application from environment vars.
    Preconfigured values are set from: RUNMODE=test or RUNMODE=production
    Each parameter can be overriden directly by an environment variable.
    """

    RUNMODE = os.environ.get("RUNMODE")
    if RUNMODE == "production":
        PGHOST = "resif-pgprod.u-ga.fr"
        PGUSER = "resifinv_ro"
        PGPORT = "5432"
        PGDATABASE = "resifInv-Prod"
        ARCHIVE_DIR = "/mnt/auto/archive/ph5"
        PH5_DIR = "/usr/local/bin"
        REDIS_HOST = "localhost"
        REDIS_PORT = 6379
        REDIS_QUEUE = "fdsnwsstat"
    elif RUNMODE == "test":
        PGHOST = "resif-pgpreprod.u-ga.fr"
        PGUSER = "resifinv_ro"
        PGPORT = "5432"
        PGDATABASE = "resifInv-Preprod"
        ARCHIVE_DIR = "/mnt/auto/archive/ph5"
        PH5_DIR = "/usr/local/bin"
        REDIS_HOST = "localhost"
        REDIS_PORT = 6379
        REDIS_QUEUE = "fdsnwsstat-test"

    try:
        PGHOST = os.environ.get("PGHOST") or PGHOST
        PGUSER = os.environ.get("PGUSER") or PGUSER
        PGPORT = os.environ.get("PGPORT") or PGPORT
        PGDATABASE = os.environ.get("PGDATABASE") or PGDATABASE
        ARCHIVE_DIR = os.environ.get("ARCHIVE_DIR") or ARCHIVE_DIR
        PH5_DIR = os.environ.get("PH5_DIR") or PH5_DIR
        SECRET_KEY = os.environ.get("SECRET_KEY") or SECRET_KEY
        DIGEST_REALM = os.environ.get("DIGEST_REALM") or DIGEST_REALM
        REDIS_HOST = os.getenv("REDIS_HOST") or REDIS_HOST
        REDIS_PORT = os.getenv("REDIS_PORT") or REDIS_PORT
        REDIS_QUEUE = os.getenv("REDIS_QUEUE") or REDIS_QUEUE
    except NameError:
        print(
            "Missing environment variables. Either RUNMODE=(test|production) or PGHOST, PGUSER, PGPORT and PGDATABASE should be set."
        )
        raise
    DATABASE_URI = f"postgresql://{PGUSER}@{PGHOST}:{PGPORT}/{PGDATABASE}"
