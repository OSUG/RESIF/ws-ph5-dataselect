# Webservice ph5-dataselect

## Play around with docker

```
docker build -t ws-ph5-dataselect .
docker run --rm -e RUNMODE=test -p 8000:8000 --name ws-ph5  ws-ph5-dataselect:latest
```

Then :

```
wget -O - http://localhost:8000/1/application.wadl
```

Run it in debug mode with flask:

```
RUNMODE=test FLASK_APP=start.py flask run
```

## RUNMODE builtin values

  * `production`
  * `test`

## Authors (dc@resif.fr)

  * Jérôme Touvier
  * Jonathan Schaeffer
  * Philippe Bollard (Maintainer)
